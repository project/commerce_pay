core = 7.x
api = 2

; USAePay PHP Library
libraries[usaepay-php][download][type] = "git"
libraries[usaepay-php][download][url] = "git@github.com:usaepay/usaepay-php.git"
libraries[usaepay-php][destination] = "libraries"
